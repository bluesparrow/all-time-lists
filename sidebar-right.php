<!-- Right sidebar -->
<aside class="sidebar col-sm-4 col-md-3" role="complementary">

  <?php if(is_single()): ?>
    <div class="sidebar-widget">
      <?php atl_recommended_posts();?>
    </div>
    <!-- END Recommended Post -->
  <?php endif; ?>

  <div class="sidebar-widget">
    <h3 class="widget-title">Search <br> <small>within our lists</small></h3>
    <?php get_template_part('searchform'); ?>
  </div>
  <!-- END Search form -->

  <div class="sidebar-widget">
    <h3 class="widget-title">Trending <br> <small>Trending lists right now</small></h3>
    <?php 
      $popularpost = new WP_Query( array( 
        'posts_per_page' => 5, 
        'meta_key' => 'wpb_post_views_count', 
        'orderby' => 'meta_value_num', 
        'order' => 'DESC'  ) );
      ?>
      
      <ul class="list-unstyled post-list">
      
      <?php while ( $popularpost->have_posts() ) : $popularpost->the_post(); ?>

        <li>
          <a href="<?php the_permalink() ?>" class="post-thumb">
          <?php if( has_post_thumbnail() ): 
            the_post_thumbnail('small');
          else: ?>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/placeholder.jpg" alt="<?php the_title();?>">
          <?php endif; ?>
          </a>

          <div class="post-info">
            <span class="category"><?php the_category(" / "); ?></span>
            <a href="<?php the_permalink() ?>" title="<?php the_title();?>" class="title">
              <?php the_title();?>
            </a>
            <span class="date"><?php the_time('M j, Y'); ?></span>
          </div>
        </li>

      <?php endwhile; ?>
    
    </ul>
  </div>
  <!-- END Most Popular -->

  <div class="sidebar-widget">
    <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-right')); ?>
  </div>
  <!-- END Dynamic sidebar widgets -->
  
</aside>
<!-- END Right Sidebar -->