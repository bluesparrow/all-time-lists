var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var plumber = require('gulp-plumber');
var notify = require("gulp-notify");
var concat = require('gulp-concat');
var cleanCSS = require('gulp-clean-css');
//var purify = require('gulp-purifycss');
var browserSync = require('browser-sync').create();

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
  return gulp.src("assets/stylesheets/**/*.scss")
    .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
    .pipe(sass({sourceComments: false, outputStyle: 'expanded'}))
    .pipe(gulp.dest("css/libs"))
    .pipe(browserSync.stream());
});

// Uglify JS
gulp.task('uglify', function() {
  return gulp.src('assets/scripts/*.js')
    .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
    .pipe(uglify())
    .pipe(gulp.dest('js'))
    .pipe(browserSync.stream());
});

// Concat CSS
gulp.task('concat-css', function() {
  return gulp.src(['css/libs/bootstrap.css', 'css/libs/ionicons.css', 'css/libs/main.css'])
    //.pipe(purify(['js/**/*.js', '**/*.php']))
    .pipe(cleanCSS({keepSpecialComments: 0}))
    .pipe(concat('main.min.css'))
    .pipe(gulp.dest('css/'));
});

// Concat JS
gulp.task('concat-js', function() {
  return gulp.src(['js/lib/jquery.min.js', 'js/bootstrap.min.js', 'js/scripts.js', 'js/ajax.js'])
    .pipe(concat('scripts.min.js'))
    .pipe(gulp.dest('js/'));
});

// Static Server + watching scss/php files
gulp.task('watch', function() {
  browserSync.init({
    proxy: "http://localhost:8888/alltimelists/"
  });

  gulp.watch("assets/stylesheets/**/*.scss", ['sass']);
  gulp.watch("assets/scripts/*.js", ['uglify']);
  gulp.watch("css/**/*.css", ['concat-css']);
  gulp.watch("js/**/*.js", ['concat-js']);
  gulp.watch("*.php").on('change', browserSync.reload);
});

gulp.task('build', ['sass', 'uglify', 'concat-css', 'concat-js']);
gulp.task('default', ['watch']);
