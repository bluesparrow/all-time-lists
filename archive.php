<?php get_header(); ?>

  <section class="col-sm-9 main-content content-loop single-col">
    
    <h1 class="page-title"><?php _e( 'Archives', 'html5blank' ); ?></h1>

    <section class="ad-wrapper">
      <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('header-ad')) ?>
    </section>
    <!-- END AD Wrapper Widget Space-->
    
    <div class="article-wrapper">

      <div class="loop-wrapper">

        <div class="row">
          <?php get_template_part('content-loop'); ?>
        </div>

        <?php get_template_part('pagination'); ?>

      </div>
      <!-- END Loop wrapper -->
      
    </div>
    <!-- END Article wrapper -->

    <?php include("partials/loading.php"); ?>
  </section>

<?php get_sidebar('right'); ?>

<?php get_footer(); ?>
