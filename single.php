<?php get_header(); ?>

	<main class="col-sm-8 col-md-9 main-content post-single">
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<header class="article-header">
					
					<div class="post-meta-wrapper">
						<div class="tm-wrapper">
							<h1 class="post-title"><?php the_title(); ?></h1>
							<!-- END Post title -->

							<div class="post-meta">							
								<span class="date"><span class="icon ion-calendar"></span> <?php the_time('M j, Y'); ?></span>
								<span class="comments"><span class="icon ion-chatbox-working"></span> <?php if (comments_open( get_the_ID() ) ) comments_popup_link( __( 'Leave your thoughts', 'html5blank' ), __( '1 Comment', 'html5blank' ), __( '% Comments', 'html5blank' )); ?></span>

								<span class="category">
									<span class="icon ion-pricetags"></span>
									<?php the_category(' , '); // Separated by commas ?>
								</span>

								<!-- mfunc W3TC_DYNAMIC_SECURITY -->
									<?php wpb_set_post_views(get_the_ID()); ?>
								<!-- /mfunc W3TC_DYNAMIC_SECURITY -->

								<!-- Hiding view count for now 
								<span class="view-counts">
									<?php echo '<span class="icon ion-android-star"></span> '.wpb_get_post_views(get_the_ID()); ?>
								</span>
								-->

								<!-- END Post View Count -->
							</div>
							<!-- END Post meta details -->
						</div>
						<!-- END Title and Meta Wrapper -->

						<div class="author-meta">
							<figure class="author-avatar">
								<?php echo get_avatar( get_the_author_meta('ID'), 60); ?>
							</figure>

							<span class="author-name">
								<?php _e( 'Written by', 'html5blank' ); ?> <br />
								<?php the_author_posts_link(); ?>
							</span>

							<span class="action edit">
								<?php edit_post_link('Edit This Post'); // Always handy to have Edit Post Links available ?>
							</span>
						</div>
						<!-- END Author meta -->

					</div>
					<!-- END Post meta wrapper -->

					<div class="post-thumbnail-wrapper">
						<?php if(has_post_thumbnail()): ?>
							<?php the_post_thumbnail(); ?>
						<?php else: ?>
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/placeholder-featured.jpg" alt="<?php the_title(); ?>">
						<?php endif; ?>
					</div>

				</header>
				<!-- END Post Header -->

				<section class="post-content">
					<?php if ( function_exists('yoast_breadcrumb') )  {yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>

					<?php the_content(); // Dynamic Content ?>

					<div class="ad-wrapper content-ad">
						<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('content-top-ad')) ?>						
					</div>
					<!-- END Content top Ad -->

					<!-- Custom fields by All time lists -->
					<?php the_field('introduction'); ?>

					<?php
						// check if the repeater field has rows of data
						if( have_rows('list_items') ):

						 	// loop through the rows of data
						    while ( have_rows('list_items') ) : the_row();
						  ?>

					        <?php 
					        //display a sub field value 
					        $title = get_sub_field('title'); 
					        $image = get_sub_field('image');
					        $alt = $image['alt'];
					        // $image_credit = get_sub_field('image_credit');
					        $description = get_sub_field('description');

					        // thumbnail
									$size = 'post';
									$thumb = $image['sizes'][ $size ];
					        ?>
					        
					        <h2><?php echo $title; ?></h2>

									<?php if( !empty($image) ): ?>
										
										<figure class="list-image">
											<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" />
										</figure>

									<?php endif; ?>

					        <?php echo $description; ?>

					        <div class="hr"></div>

						<?php
						    endwhile;

						else :

							// no rows found
							echo "No items to display";

						endif;
						?>

						<h3>Conclusion</h3>
						<?php the_field('conclusion'); ?>
				</section>
			</article>
			<!-- /article -->

			<div class="tagcloud">
				<?php the_tags( __( '<span>Tags:</span> ', 'html5blank' ), '', ''); // Separated by commas with a line break at the end ?>
			</div>

			<div class="ad-wrapper content-ad">
				<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('content-bottom-ad')) ?>						
			</div>
			<!-- END Content Bottom Ad -->
			
			<?php //include("partials/newsletter.php"); ?>

			<?php get_template_part('partials/related-posts'); ?>

			<?php comments_template(); ?>

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

			</article>
			<!-- /article -->

		<?php endif; ?>

	</main>

	<?php get_sidebar('right'); ?>

<?php get_footer(); ?>
