<?php
/**
 * Template Name: Custom RSS Template - Feedname
 */
$postCount = 10; // The number of posts to show in the feed
$posts = query_posts('showposts=' . $postCount);
header('Content-Type: '.feed_content_type('rss-http').'; charset='.get_option('blog_charset'), true);
echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>';
?>
<rss version="2.0"
    xmlns:content="http://purl.org/rss/1.0/modules/content/"
    xmlns:wfw="http://wellformedweb.org/CommentAPI/"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:atom="http://www.w3.org/2005/Atom"
    xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
    xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
    <?php do_action('rss2_ns'); ?>>
<channel>
    <title><?php bloginfo_rss('name'); ?></title>
    <atom:link href="<?php self_link(); ?>" rel="self" type="application/rss+xml" />
    <link><?php bloginfo_rss('url') ?></link>
    <description><?php bloginfo_rss('description') ?></description>
    <lastBuildDate><?php echo mysql2date('D, d M Y H:i:s +0000', get_lastpostmodified('GMT'), false); ?></lastBuildDate>
    <language><?php echo get_option('rss_language'); ?></language>
    <sy:updatePeriod><?php echo apply_filters( 'rss_update_period', 'hourly' ); ?></sy:updatePeriod>
    <sy:updateFrequency><?php echo apply_filters( 'rss_update_frequency', '1' ); ?></sy:updateFrequency>
    <?php do_action('rss2_head'); ?>
    <?php while(have_posts()) : the_post(); ?>
        <item>
            <title><?php the_title_rss(); ?></title>
            <link><?php the_permalink_rss(); ?></link>
            <pubDate><?php echo mysql2date('D, d M Y H:i:s +0000', get_post_time('Y-m-d H:i:s', true), false); ?></pubDate>
            <dc:creator><?php the_author(); ?></dc:creator>
            <guid isPermaLink="false"><?php the_guid(); ?></guid>
            <description>
            <![CDATA[<?php echo wp_trim_words( get_field('introduction'), 30, '...' );?>]]>
            </description>
            
            <content:encoded>
                <![CDATA[<?php echo get_field('introduction');?>]]>

                <![CDATA[
                <figure class="op-ad">
                    <iframe width="300" height="250" style="border:0; margin:0;" src="https://www.facebook.com/adnw_request?placement=1401203206572723_1780729285286778&adtype=banner300x250"></iframe>
                </figure>
                ]]>

                <?php
                // check if the repeater field has rows of data
                if( have_rows('list_items') ):

                    // loop through the rows of data
                    while ( have_rows('list_items') ) : the_row(); ?>

                        <?php 
                        //display a sub field value 
                        $title = get_sub_field('title'); 
                        $image = get_sub_field('image');
                        $alt = $image['alt'];
                        $description = get_sub_field('description');

                        // thumbnail
                        $size = 'post';
                        $thumb = $image['sizes'][ $size ];
                        ?>
                        
                        <![CDATA[<h2><?php echo $title; ?></h2>]]>

                        <?php if( !empty($image) ): ?>
                        
                        <![CDATA[<figure class="list-image"><img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" /></figure>]]>

                        <?php endif; ?>

                        <![CDATA[<?php echo $description; ?>]]>

                        <?php
                    endwhile;
                endif;
                ?>

                <?php $related = atl_related_posts(); ?>
                <?php if ( $related->have_posts() ): ?>
                    <![CDATA[<p><strong>See Also:</strong></p>]]>
                    <?php while ( $related->have_posts() ) : $related->the_post(); ?>
                        <![CDATA[<p><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></p>]]>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>
                <?php wp_reset_query(); ?>

                <![CDATA[ <p>Read more on our website <a href="<?php the_permalink_rss() ?>">All time lists</a> .</p> ]]>
            </content:encoded>

            <?php rss_enclosure(); ?>
            <?php do_action('rss2_item'); ?>
        </item>
    <?php endwhile; ?>
</channel>
</rss>