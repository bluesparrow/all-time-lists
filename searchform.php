<!-- search -->
<form method="get" action="<?php echo home_url(); ?>" role="search" class="sidebar-searchform">
  <div class="input-group">
    <input class="form-control" type="search" name="s" placeholder="<?php _e( 'Search ..', 'html5blank' ); ?>">
    <span class="input-group-btn">
      <button class="btn btn-brand" type="submit" role="button"><?php _e( 'Search', 'html5blank' ); ?></button>
    </span>
  </div><!-- /input-group -->
</form>
<!-- /search -->
