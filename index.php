<?php get_header(); ?>

    <?php get_sidebar('left'); ?>

    <main role="main" class="col-md-6 col-sm-8 main-content content-loop single-col">

      <section class="ad-wrapper">
        <?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('header-ad')) ?>
      </section>
      <!-- END AD Wrapper Widget Space-->
      
			<section class="article-wrapper">
        
        <div class="loop-wrapper">

          <div class="row">
            <?php get_template_part('content-loop'); ?>
          </div>

    			<?php get_template_part('pagination'); ?>

        </div>
        <!-- END Loop wrapper -->

      </section>
      <!-- END Article wrapper -->

      <?php include("partials/loading.php"); ?>

  	</main>
    <!-- END Main content -->

  <?php get_sidebar('right'); ?>

<?php get_footer(); ?>
