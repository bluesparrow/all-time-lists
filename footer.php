				</div>
			</section>
			<!-- END Content wrapper -->
			
			<!-- footer -->
			<footer class="footer" role="contentinfo">
				<div class="container">
					<div class="block-table">
						<div class="block-table-cell">
							<nav class="footer-nav">
								<?php footer_nav(); ?>
							</nav>

							<p class="copyright">
								Copyright <?php echo date('Y'); ?> &copy; <?php bloginfo('name'); ?>. All right reserved
							</p>
						</div>

						<div class="block-table-cell footer-logo">
							All time lists
						</div>
					</div>
					
				</div>
			</footer>
			<!-- /footer -->

			<a href="#" class="go-top">
				<span class="icon ion-chevron-up"></span> &nbsp;
				GO TO TOP
			</a>

		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

		<!-- analytics -->
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-73275568-1', 'auto', {
				anonymizeIp: true
			});

			ga('send', 'pageview');
		</script>

	</body>
</html>
