<?php if (have_posts()): while (have_posts()) : the_post(); ?>

  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="post-wrapper block-table">
      <figure class="post-thumb block-table-cell">
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
          <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
            <?php the_post_thumbnail('thumbnail'); // Declare pixel size you need inside the array ?>
          <?php else: ?>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/placeholder.jpg" alt="<?php the_title(); ?> - All time lists">
          <?php endif; ?>
        </a>
        
        <?php if ( comments_open() ): ?>
          <p class="comments-count">
            <a class="post-comments" href="<?php comments_link(); ?>"><?php comments_number( '0', '1', '%' ); ?></a>
          </p>
        <?php endif; ?>
      </figure>
      <!-- END Post thumbnail -->

      <div class="post-content block-table-cell">
        <header class="post-header">
          <p class="post-category"><?php the_category( ' / ' ); ?></p>
          <!-- END Post category -->

          <h2 class="post-title">
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
          </h2>
          <!-- END Post title -->
        </header>

        <div class="meta-wrapper clearfix">
          <span class="post-date"><span class="icon ion-calendar"></span> <?php the_time('M j, Y'); ?></span>
          <span class="post-author"><span class="icon ion-person"></span> <?php the_author_posts_link(); ?></span>
        </div>

        <div class="post-excerpt">
          <?php echo wp_trim_words( get_field('introduction'), 30, '...' ); ?>
        </div>
      </div>
    </div>

  </article>
  <!-- / END article -->

<?php endwhile; ?>

<?php else: ?>

  <article class="pagenotfound">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <h1><span class="large-text">Uh-oh!</span> <?php _e( 'Looks like something is wrong. We couldn\'t find what you were looking for.', 'html5blank' ); ?></h1>
        <a href="<?php echo home_url(); ?>" class="btn btn-brand"><?php _e( 'Return Home?', 'html5blank' ); ?></a>

        <div class="divider-block">
          <span>or</span>
        </div>
        
        <h4>Would you like to try searching again?</h4>
        <?php include "searchform.php"; ?>
        
      </div>
    </div>
  </article>
  <!-- /END article -->

<?php endif; ?>
