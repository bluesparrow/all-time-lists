<ul class="list-unstyled comments-list">
  <?php
    $args = array(
      'status' => 'approve',
      'number' => '5',
      'orderby' => 'comment_date',
      'order' => 'DESC',
      'post_status'=>'publish'
    );

    $comments = get_comments($args);
    foreach($comments as $comment) :?>
    <li>
      <a href="<?php echo get_comment_link($comment->comment_ID); ?>" class="avatar">
        <?php echo get_avatar( $comment, 40 ); ?>
      </a>
      
      <div class="comment-info">
        <a href="<?php echo get_comment_link($comment->comment_ID); ?>">
          <span class="text">
            <strong class="author"><?php echo ($comment->comment_author); ?></strong> 
          says</span>
          <span class="content">
            <?php echo wp_trim_words($comment->comment_content, 10) ?>
          </span>
          <span class="text">on</span>
          <span class="title">
            <?php echo get_the_title($comment->comment_post_ID); ?>
          </span>
        </a>
      </div>
    </li>
    <?php endforeach; ?>
</ul>