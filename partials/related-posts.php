<div class="related-posts">
	<?php $related = atl_related_posts(); ?>

	<?php if ( $related->have_posts() ): ?>

	<h4 class="heading">
		<i class="fa fa-hand-o-right"></i><?php _e('You may also like...','html5blank'); ?>
	</h4>

	<div class="row">
		
		<?php while ( $related->have_posts() ) : $related->the_post(); ?>
		<div class="col-sm-4 item">
			<article <?php post_class(); ?>>

				<div class="post-thumb">
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
						<?php if ( has_post_thumbnail() ): ?>
							<?php the_post_thumbnail('related'); ?>
						<?php else: ?>
							<img src="<?php echo get_template_directory_uri(); ?>/img/placeholder.jpg" alt="<?php the_title(); ?>" />
						<?php endif; ?>
					</a>
				</div><!-- END Post thumbnail-->
				
				<div class="related-inner">
					
					<h4 class="post-title">
						<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>
					</h4><!-- END Post title-->
					
					<div class="post-meta group">
						<p class="post-date"><?php the_time('j M, Y'); ?></p>
					</div><!-- END Post meta-->
				
				</div><!-- END Related-inner-->

			</article>
		</div><!-- END Column -->
		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>

	</div><!-- END Row -->
	<?php endif; ?>

	<?php wp_reset_query(); ?>
</div>
<!-- END Related Posts -->