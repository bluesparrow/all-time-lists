<script type="text/javascript">
  //<![CDATA[
  if (typeof newsletter_check !== "function") {
  window.newsletter_check = function (f) {
      var re = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-]{1,})+\.)+([a-zA-Z0-9]{2,})+$/;
      if (!re.test(f.elements["ne"].value)) {
          alert("The email is not correct");
          return false;
      }
      for (var i=1; i<20; i++) {
      if (f.elements["np" + i] && f.elements["np" + i].required && f.elements["np" + i].value == "") {
          alert("");
          return false;
      }
      }
      if (f.elements["ny"] && !f.elements["ny"].checked) {
          alert("You must accept the privacy statement");
          return false;
      }
      return true;
  }
  }
  //]]>
</script>

<!-- Newsletter -->
<div class="newsletter-form">
  <div class="inner-wrapper">
    <h3>Did you enjoy reading this post?</h3>
    <p>Subscribe to our newsletter for more awesome list like this every week.</p>

    <form method="post" action="/?na=s" onsubmit="return newsletter_check(this)" class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="input-group">
          <input type="email" name="ne" size="40" class="form-control input-lg newsletter-email" placeholder="Enter your email" required>
          <span class="input-group-btn">
            <button class="btn btn-brand btn-lg newsletter-submit" type="submit">Subscribe</button>
          </span>
        </div><!-- /input-group -->
      </div>
    </form>
  </div>
</div>