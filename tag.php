<?php get_header(); ?>

  <section class="col-sm-8 col-md-9 main-content content-loop single-col">
    
    <h1 class="page-title"><?php _e( 'Tag: ', 'html5blank' ); echo single_tag_title('', false); ?></h1>
    
    <div class="article-wrapper">

      <div class="loop-wrapper">

        <div class="row">
          <?php get_template_part('content-loop'); ?>
        </div>

        <?php get_template_part('pagination'); ?>

      </div>
      <!-- END Loop wrapper -->
      
    </div>
    <!-- END Article wrapper -->

    <?php include("partials/loading.php"); ?>
  </section>

<?php get_sidebar('right'); ?>

<?php get_footer(); ?>
