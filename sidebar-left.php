<!-- Left sidebar -->
<aside class="sidebar col-sm-3 hidden-sm hidden-xs" role="complementary">

  <div class="sidebar-widget tab-widget">
   <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
      <li class="active"><a href="#tab-popular" role="tab" data-toggle="tab">Popular</a></li>
      <li><a href="#tab-recent" role="tab" data-toggle="tab">New</a></li>
      <li><a href="#tab-comments" role="tab" data-toggle="tab">Comments</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane fade in active" id="tab-popular">
        <?php post_list('comment_count'); ?>
      </div>

      <div role="tabpanel" class="tab-pane fade" id="tab-recent">
        <?php post_list('date'); ?>
      </div>

      <div role="tabpanel" class="tab-pane fade" id="tab-comments">
        <?php include("partials/comments-list.php"); ?>
      </div>
    </div>
  </div>

	<div class="sidebar-widget">
		<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-left')) ?>
	</div>

</aside>
<!-- /sidebar -->