<?php get_header(); ?>

	<section class="col-sm-8 col-md-9 main-content">

		<!-- article -->
		<article id="post-404" class="pagenotfound">

			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					
					<h1><span class="large-text">Uh-oh!</span> <?php _e( 'Looks like something is wrong. We couldn\'t find that page.', 'html5blank' ); ?></h1>
					<a href="<?php echo home_url(); ?>" class="btn btn-brand"><?php _e( 'Return Home?', 'html5blank' ); ?></a>

					<div class="divider-block">
						<span>or</span>
					</div>
					
					<h4>Would you like to use our search?</h4>
					<?php include "searchform.php"; ?>
					
				</div>
			</div>

		</article>
		<!-- /article -->

	</section>

<?php get_sidebar('right'); ?>

<?php get_footer(); ?>
