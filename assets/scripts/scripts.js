(function ($, root, undefined) {
  
  $(function () {
    
    'use strict';

    // DOM ready, take it away
    mobileMenu();
    equalHeightHeaderBottom();
    //getFeaturedImage();
    goTop();
    $(objectFitImages);

    // Enabled BS Tooltip
    $('[data-toggle="tooltip"]').tooltip()

    $(window).resize(function(){
      $(".header-bottom .social-icons").removeAttr("style");
      equalHeightHeaderBottom();
    });
    
    // Short Circuiting
    // http://stackoverflow.com/questions/11069278/javascript-if-else-shorthand
    // sidebarHeight > mainHeight && mainContent.css("height", sidebarHeight);

    // Mobile menu
    function mobileMenu(){
      $(window).on('resize', function() {
        $("#top-menu .menu-item-has-children .sub-menu").removeAttr("style");
      });

      $("#top-menu .menu-item-has-children > a").click(function(e){
        if( $(window).width() < 768 ){
          $(this).siblings('ul').slideToggle();
          return false;
        }
      });
    }

    function equalHeightHeaderBottom(){
      var browserWidth = $(window).width();
      var headerBottomHeight = $(".header-bottom").height();
      
      if( browserWidth > 767 ){
        $(".header-bottom .social-icons").height(headerBottomHeight);
      }
    }

    function getFeaturedImage(){
      var imgSource = $(".post-thumbnail-wrapper img").attr("src");
      $(".article-header").css("background-image", "url('"+imgSource+"')");
    }

    function goTop(){
      $(window).scroll(function(){
        var windowOffset = $(window).scrollTop();

        if( windowOffset > 200){
          $(".go-top").fadeIn(200);
        }else {
          $(".go-top").fadeOut(200);
        }
      });

      $(".go-top").click(function(){
        $('html, body').animate({ scrollTop: "0" });
        return false;
      });
    }

  });
  
})(jQuery, this);