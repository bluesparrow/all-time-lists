(function($) {

  // Dynamic sidebar height
  // sidebarHeight();

  // Checks and Set Sidebar height
  var checkAndSetSidebar = setInterval(function(){

    var mainContent = $(".main-content").height();
    var sidebar = $(".sidebar").height();

    if(mainContent > sidebar){
      $('.sidebar').removeAttr('style');
      sidebarHeight();
    }
  });

  // Once the page is loaded 
  // stop checking sidebar height
  $(window).load(function(){
    setTimeout(function(){
      clearInterval(checkAndSetSidebar);
    }, 500);
  });

  // Run function on resize
  $(window).resize(function(){
    $('.sidebar').removeAttr('style');
    sidebarHeight();
  });

  function sidebarHeight(){
    var browserWidth = $(window).width();
    var containerHeight = $('.content-wrapper').height();
    
    if( browserWidth < 768 ){
      $('.sidebar').removeAttr('style');
    } else {
      $('.sidebar').height(containerHeight);
    }
  }

  // Ajax loading pagination
  $('.article-wrapper').on('click', '.pagination a', function(e){
      
      e.preventDefault();

      // Display Loading image
      $('#loading').fadeIn(500);

      // Next Page link
      var link = $(this).attr('href');

      // Checks and Set Sidebar height
      var checkAndSetSidebar = setInterval(function(){
        var mainContent = $(".main-content").height();
        var sidebar = $(".sidebar").height();

        if(mainContent > sidebar){
          $('.sidebar').removeAttr('style');
          sidebarHeight();
        }
      });

      $('.article-wrapper').fadeOut(500, function(){

          $(this).load(link + ' .loop-wrapper', function() {
            
            // Loaded Page
            $(this).fadeIn(500);
            
            // Hide Loading image
            $('#loading').fadeOut(500);
            
            // Change sidebar height on pagination
            $('.sidebar').removeAttr('style');
            sidebarHeight();

            // clearInterval
            setTimeout(function(){
              clearInterval(checkAndSetSidebar);
            }, 1200);

          });
        
      }); // END AJAX Load

      // Animate page scroll
      $('body').animate({ scrollTop: 0 }, 500);
  });

  // Disqus AJAX Scripts
  disqus_config = function() {
    /* Available callbacks are afterRender, onInit, onNewComment, 
    onPaginate, onReady, preData, preInit, preReset */
    
    // On Disqus load
    this.callbacks.onReady.push(function(){
      //console.log("Disqus has loaded");

      setInterval(function(){
        //console.log("Updating sidebar for disqus");

        var mainContent = $(".main-content").height();
        var sidebar = $(".sidebar").height();

        if(mainContent > sidebar){
          $('.sidebar').removeAttr('style');
          sidebarHeight();
        }
      });
    });
  }

})(jQuery);