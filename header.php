<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="p:domain_verify" content="afab32551202cfc6c194fa5f06a58da2"/>

		<title><?php wp_title(''); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?>" href="http://feeds.feedburner.com/AllTimeLists" />

		<?php wp_head(); ?>

	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<script>
		(adsbygoogle = window.adsbygoogle || []).push({
			google_ad_client: "ca-pub-8664130312047964",
			enable_page_level_ads: true
		});
	</script>
	</head>
	<body <?php body_class(); ?>>

		<!-- wrapper -->
		<div class="wrapper">

			<!-- header -->
			<header class="header">

				<nav class="navbar navbar-inverse">
					<div class="container">
				    <!-- Brand and toggle get grouped for better mobile display -->
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-menu" aria-expanded="false">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				      <h1 class="logo">
					      <a class="navbar-brand" href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a>
				      </h1>
				    </div>

				    <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse" id="top-menu">
				      <?php main_nav(); ?>
				    </div><!-- /.navbar-collapse -->
			    </div>
				</nav>
				
				<div class="header-bottom">
					<div class="container">
						<div class="row">
							<nav class="hb-nav col-sm-8 col-md-9 second-menu">
								<?php secondary_nav(); ?>
							</nav>

							<nav class="hb-nav col-sm-4 col-md-3 social-icons">
								<ul class="list-inline">
									<li><a href="http://facebook.com/alltimelists" target="_blank" title="Find us in Facebook"><span class="icon ion-social-facebook"></span></a></li>
									<li><a href="http://twitter.com/alltimelists" target="_blank" title="Follow us in Twitter"><span class="icon ion-social-twitter"></span></a></li>
									<li><a href="http://google.com/+Alltimelistsdotcom" target="_blank" title="Follow us in Google Plus"><span class="icon ion-social-googleplus"></span></a></li>
									<li><a href="https://www.instagram.com/alltimelists" target="_blank" title="Follow us in Instagram"><span class="icon ion-social-instagram-outline"></span></a></li>
									<li><a href="?random=1" data-toggle="tooltip" data-placement="bottom" title="Random Post" data-container="body"><span class="icon ion-shuffle"></span></a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
				
			</header>
			<!-- /header -->

			<section class="container content-wrapper">
				<div class="row">